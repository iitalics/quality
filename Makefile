SRC=src

OC=ocamlbuild -I $(SRC) -use-ocamlfind

all: main

main: main.native
	mv main.native qcc

main.native:
	$(OC) $@

test-%.c: tests/%.ql
	./qcc -o $@ -I stdlib $<

tests: test-hello.c test-rec.c test-loop.c

clean:
	rm -rf _build qcc $(wildcard test-*.c)

.PHONY: all main.native clean
