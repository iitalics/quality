printfd :: str int -> unit
printfd = external "printf"

fib :: int -> int
fib(n) = {
  var a = 0
  var b = 1

  while n > 0:
    var c = a + b
    a = b
    b = c
    n = n - 1
  ;;

  a
}

main :: -> int
main = {
  printfd("fib(10) = %d\n", fib(10))
  0
}