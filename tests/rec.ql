printfd :: str int -> unit
printfd = external "printf"

fib :: int -> int
fib(n) = {
  if n == 0:
    0
  elif n == 1:
    1
  else:
    fib(n - 1) + fib(n - 2)
  ;;
}

main :: -> int
main = {
  printfd("fib(10) = %d\n", fib(10))
  0
}